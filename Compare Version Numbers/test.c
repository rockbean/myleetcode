#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compareVersion(char* version1, char* version2) {
	printf("ver1 %s ver2 %s\n", version1, version2);
	if (atoi(version1) > atoi(version2)) {
		return 1;
	} else if (atoi(version1) < atoi(version2)) {
		return -1;
	} else {
		char *sub1 = strstr(version1, ".");
		char *sub2 = strstr(version2, ".");
		if (sub1 == NULL && sub2 == NULL)
			return 0;
		else if (sub1 == NULL && sub2) {
			if (atoi(sub2+1) == 0) return 0;
			return -1;
		}
		else if (sub1 && sub2 == NULL) {
			if (atoi(sub1+1) == 0) return 0;
			return 1;
		}
		else
			return compareVersion(sub1+1, sub2+1);
	}
}

int main(void)
{
	int ret = compareVersion("1", "1.1");
	printf("ret %d\n",ret);
	return 0;
}