#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>


typedef struct {
    int max;
	int used;
	int min_val;
	int *val_array;
} MinStack;

void minStackCreate(MinStack *stack, int maxSize) {
    stack->max = maxSize;
	stack->used = maxSize;
	stack->min_val = INT_MAX;
	stack->val_array = (int *)calloc(sizeof(int), maxSize);
}

void minStackPush(MinStack *stack, int element) {
	if (stack->used > 0) {
		stack->used--;
		stack->val_array[stack->used] = element;
		if (element < stack->min_val)
			stack->min_val = element;
	}
}

void minStackPop(MinStack *stack) {
    if (stack->used < stack->max) {
		stack->used++;
	}
	
	int i;
	for(i=stack->used;i<stack->max;i++) {
		if (stack->val_array[i] < stack->min_val)
			stack->min_val = stack->val_array[i];
	}
}

int minStackTop(MinStack *stack) {
    return stack->val_array[stack->used];
}

int minStackGetMin(MinStack *stack) {
    return stack->min_val;
}

void minStackDestroy(MinStack *stack) {
    free(stack->val_array);
}

void StackPrint(MinStack *stack)
{
	int i;
	printf("===> ");
	for(i=stack->used;i<stack->max;i++) {
		printf("%d ", stack->val_array[i]);
	}
	printf("\n");
}

int main(void)
{
	MinStack stack;
	minStackCreate(&stack, 10);
	
	minStackPush(&stack, 2147483646);
	minStackPush(&stack, 2147483646);
	minStackPush(&stack, 2147483647);
	printf("top %d\n", minStackTop(&stack));
	minStackPop(&stack);
	printf("min %d\n", minStackGetMin(&stack));
	minStackPop(&stack);
	printf("min %d\n", minStackGetMin(&stack));
	minStackPop(&stack);
	minStackPush(&stack, 2147483647);
	printf("top %d\n", minStackTop(&stack));
	printf("min %d\n", minStackGetMin(&stack));
	minStackPush(&stack, -2147483648);
	printf("top %d\n", minStackTop(&stack));
	printf("min %d\n", minStackGetMin(&stack));
	minStackPop(&stack);
	printf("min %d\n", minStackGetMin(&stack));
	StackPrint(&stack);
	minStackDestroy(&stack);
	return 0;
}