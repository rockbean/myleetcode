#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *convert(char *s, int nRows) {
    int size = strlen(s);
	char *tmp = (char *)calloc(1, size+1);
	int i = 0, j = 0, max = nRows-1, num = 0;
	
	if (max <= 0 || max >= size) {
		strcpy(tmp, s);
		return tmp;
	}
	
	for(i=0;i<nRows;i++) {
		num = i;
		while(num < size) {
			if (j == size)
				break;
				
			tmp[j++] = s[num];

			if (j<size && i != 0 && i != max && (num + (max - i)*2) < size) {
				tmp[j++] = s[num + (max - i)*2];
			}
			num += max*2;
		} 
	}
	return tmp;
}

int main(void)
{
	char *str = convert("AB", 2);
	printf("str=> %s---\n", str);
	free(str);
	return 0;
}