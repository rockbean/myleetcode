#include <stdio.h>
#include <string.h>

int char_to_num(char *c)
{
	if (*c < 'A' || *c > 'Z')
		return 0;
	switch(*c) {
		case 'A':
			return 1;
		default:
			return (int)(*c - 'A' + 1);
	}
}

int power(int num, int count)
{
	int sum = 1, i;
	for (i=0;i<count;i++) {
		sum *= num;
	}
	return sum;
}

int titleToNumber(char* s) {
    int size = strlen(s);
	int i, ret = 0;
	for (i=0;i<size;i++) {
		ret += power(26, size-i-1) * char_to_num(s+i);
	}
	return ret;
}

int main(void)
{
	char *z = "A&B";
	int ret = titleToNumber(z);
	printf("ret %d\n", ret);
	return 0;
}