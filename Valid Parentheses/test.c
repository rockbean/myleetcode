typedef struct node_t
{
	int flag;	// 1 - {}, 2 - [], 3 - ()
	struct node_t *next;
} node_t;

node_t *add_to_head(node_t *tree, int flag)
{
	node_t *node = (node_t *)malloc(sizeof(node_t));
	node->flag = flag;	
	node_t *tmp = tree;
	node->next = tmp;
	tree = node;
	return tree;
}

node_t *free_head(node_t *tree)
{
	node_t *tmp = tree;
	if (tmp) {
		tree = tree->next;
		free(tmp);
	}
	return tree;
}

bool isValid(char* s) {
	int ret = 1;
	node_t *tree = NULL;
	char *cur = s;
	
	while(*cur != '\0') {
		if (*cur != '{' && *cur != '[' && *cur != '(' &&
			*cur != '}' && *cur != ']' && *cur != ')') {
			cur++;continue;
		}
		
		if (*cur == '{') {			
			tree = add_to_head(tree, 1);
		} else if (*cur == '[') {
			tree = add_to_head(tree, 2);
		} else if (*cur == '(') {
			tree = add_to_head(tree, 3);
		} else if (*cur == '}') {
			if (tree && tree->flag == 1) {
				tree = free_head(tree);
			} else {
				ret = 0;
				break;
			}
		} else if (*cur == ']') {
			if (tree && tree->flag == 2) {
				tree = free_head(tree);
			} else {
				ret = 0;
				break;
			}
		} else if (*cur == ')') {
			if (tree && tree->flag == 3) {
				tree = free_head(tree);
			} else {
				ret = 0;
				break;
			}
		}
		
		cur++;
	}
	
	while(tree) {
		ret = 0;
		node_t *tmp = tree;
		tree = tree->next;
		free(tmp);
	}

	return ret;
}