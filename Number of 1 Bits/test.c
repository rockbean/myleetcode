#include <stdio.h>
#include <stdint.h>
int hammingWeight(uint32_t n) {
    int count = 0;
	for (count=0; n; count++) {
		printf("before n %d\n", n);
        n &= n-1;
		printf("after n %d\n", n);
	}	
    return count;
}

int main(void)
{
	int ret = hammingWeight(4);
	printf("ret %d\n", ret);
}