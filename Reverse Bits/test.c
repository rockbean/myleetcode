#include <stdio.h>
#include <stdint.h>

uint32_t reverseBits(uint32_t n) {
	int tmp = 0, count = 0;
	uint32_t x = 0;
	while(n > 0){
		tmp = n%2;
		x |= tmp;
		n = n/2;
		if (n > 0)
			x = x << 1;
		count++;
	}
	if (count < 32)
		x = x << (32 - count);
	return x;
}

int main(void)
{
	uint32_t x = reverseBits(43261596);
	printf("==> %d\n", x);
	return 0;
}