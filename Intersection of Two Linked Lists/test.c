#include <stdio.h>
#include <stdlib.h>

struct ListNode {
	int val;
	struct ListNode *next;
};

struct ListNode *create(int start, int size)
{
	struct ListNode *tree = NULL;
	int i;
	for (i=0;i<size;i++) {
		struct ListNode *node = (struct ListNode *)malloc(sizeof(struct ListNode));
		node->val = start + i;
		node->next = tree;
		tree = node;
	}
	return tree;
}

void destroy(struct ListNode *tree)
{
	struct ListNode *tmp = tree;
	while(tree) {
		tmp = tree->next;
		free(tree);
		tree = tmp;
	}
}

void display(struct ListNode *tree)
{
	struct ListNode *node = tree;
	while(node) {
		printf("->%d", node->val);
		node = node->next;
	}
	printf("---\n");
}

int length(struct ListNode *tree)
{
	int len = 0;
	struct ListNode *node = tree;
	while(node) {
		len++;
		node = node->next;
	}
	return len;
}

struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
	
	if (headA == NULL || headB == NULL) return NULL;
	int lenA = length(headA);
	int lenB = length(headB);
	struct ListNode *nodeA = headA;
	struct ListNode *nodeB = headB;
	struct ListNode *ret = NULL;
	
	while(lenA > lenB) {
		nodeA = nodeA->next;
		lenA--;
	}
	
	while(lenB > lenA) {
		nodeB = nodeB->next;
		lenB--;
	}
		
	while(nodeA != nodeB) {
		nodeA = nodeA->next;
		nodeB = nodeB->next;
	}
	
    return nodeA;
}

int main(void)
{
	
	struct ListNode *atree = NULL, *btree = NULL, *ctree = NULL;
	
	atree = create(1, 10);
	btree = create(2, 1);
	ctree = getIntersectionNode(atree, btree);
	printf("atree: ");display(atree);
	printf("btree: ");display(btree);
	printf("ctree: ");display(ctree);
	
	destroy(atree);
	destroy(btree);
	return 0;
}