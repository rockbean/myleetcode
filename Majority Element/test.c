#include <stdio.h>

int majorityElement(int num[], int n) {
    int i = 0;
    int result = 0;
    int count = 0;
    for (;i < n; ++i) {
        if (count == 0 || result == num[i]) {
            result = num[i];
            count++;
        } else {
            count--;
        }
    } 
    return result;
}

int main(void)
{
	int num[] = {1, 2, 2, 3, 4};
	int ret = majorityElement(num, 5);
	printf("ret %d\n", ret);
	return 0;
}