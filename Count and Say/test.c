int cnt(char *str, char flag)
{
	int count = 0;
	char *pos = str;
	while (*pos == flag) {
		count++;
		pos++;
		if (*pos == '\0')
			break;
	}
	return count;
}

int decimal_digit(int n)
{
	int digit = 0;
	do {
		n = n / 10;
		digit++;
	} while(n);
	return digit;
}

char* countAndSay(int n) {
	int i, max_size = 32;
	int times = 0;
	char *str = (char *)calloc(1, max_size);
	
	while(times < n) {
		char *pos = str;
		char *tmp = (char *)calloc(1, max_size);
		char *ss = tmp;
		int len = 0;
		do{
			int count = cnt(pos, *pos);
			len += decimal_digit(count) + 1;
			if (len >= max_size) {
				max_size *= 2;
				str = (char *)realloc(str, max_size);
				tmp = (char *)realloc(tmp, max_size);
				ss = tmp + len - decimal_digit(count) - 1;
			}
			sprintf(ss, "%d%c", count, *pos);
			ss = tmp + len;
			pos += count;
		} while (*pos != '\0');
		strcpy(str, tmp);
		free(tmp);
		times++;
	}
	
	return str;
}