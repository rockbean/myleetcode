def digSum(i):
    sum=0
    while i>0:
        sum+=(i%10)**2
        i=(i-i%10)/10
    return sum
    

def check(i):
    temp=[]
    while (not i in temp):
        temp.append(i)
        i=digSum(i)
    temp.append(i)
    return temp


for i in range(1,100):
    print i,'has orbit',check(i)
    
    
