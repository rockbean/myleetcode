#include <stdio.h>
#include <string.h>
typedef int bool;

int array[1000];

int calc(int n )
{
	int r=0;
    while(n>0) {
        r+= (n%10)*(n%10);
        n /= 10;
    }
    return r;
}

int happy(int n) {
    int r=0;
	printf("n %d array[n] %d\n", n, array[n]);
	if(n >= 1000)n=happy(calc(n));
	if(array[n] == 2) return 0;
    if(array[n] != -1)return array[n];
	array[n] = 2;
	
	r = happy(calc(n));
    return r;
}

bool isHappy(int n) 
{
    memset(array, 0xff, 1000 * sizeof(int));
	array[1] = 1;
	
	return happy(calc(n));
}
int main(void)
{
	int n = 1578943652;
	bool ret = isHappy(n);
	printf("%d %s happy\n", n, ret?"is":"isnot");
	return 0;
}