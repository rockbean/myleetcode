#include <stdio.h>

int trailingZeroes(int n) {
    int ret = 0;
	while( n / 5) {
		ret += n / 5;
		n = n/5;
	}
	return ret;
}

int main(void)
{
	int ret = trailingZeroes(100);
	printf("ret %d\n",ret);
	return 0;
}