#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int* getRow(int rowIndex, int* returnSize) {
	rowIndex += 1;
    int *array = (int *)calloc(sizeof(int), rowIndex);
	if (array == NULL) {
		*returnSize = 0;
		return NULL;
	}
	array[0] = 1;
	
	int i, j;
	for(i=0;i<rowIndex;i++) {
		for(j=i;j>0;j--) {
			array[j] = array[j] + array[j-1];
		}
	}
	
	*returnSize = rowIndex;
	return array;
}

int main(void)
{
	int size = 0;
	int *atr = getRow(8, &size);
	int i;
	
	printf("[");
	for(i=0;i<size;i++) {
		printf("%d ", atr[i]);
	}
	printf("]\n");
	free(atr);
	return 0;
}