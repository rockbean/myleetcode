#include <stdio.h>
#include <string.h>

typedef int bool;

typedef struct map_t
{
	char src;
	char dst;
} map_t;

bool isIsomorphic(char* s, char* t) {
	if (!s || !t) return 0;
	if (strlen(s) != strlen(t)) return 0;
    int size = strlen(s);
	bool ret = 1;
	
	map_t map[256] = {{0, 0}};
	int i = 0;
	while(s[i] != '\0') {
		if (map[s[i]].src == 0) {
			map[s[i]].src = s[i];
			map[s[i]].dst = t[i];
		} else if (map[s[i]].dst != t[i]) {
			return 0;
		}
		i++;
	}
	bzero(map, 256 * sizeof(map_t));
	i = 0;
	while(t[i] != '\0') {
		if (map[t[i]].src == 0) {
			map[t[i]].src = t[i];
			map[t[i]].dst = s[i];
		} else if (map[t[i]].dst != s[i]) {
			return 0;
		}
		i++;
	}
	return ret;
}

int main(void)
{
	int ret = isIsomorphic("ab", "ca");
	printf("ret %d\n", ret);
	return 0;
}