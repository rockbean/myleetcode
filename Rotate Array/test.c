#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void R(int *a, int n){
	int i;
    for(i=0;i<n/2;i++){
        int temp = a[i];
        a[i] = a[n-i-1];
        a[n-i-1] = temp;
    }
}
void rotate_2(int nums[], int n, int k) {
    k = k%n;
    R(nums,n-k);
    R(nums+n-k,k);
    R(nums,n);
}

void rotate(int nums[], int n, int k) {
	if (n<=0)
		return;
    int *tmp = (int *)calloc(n, sizeof(int));
	k = k % n;
	memcpy(tmp + k, nums, (n - k) * sizeof(int));
	memcpy(tmp, nums + n - k, k * sizeof(int));
	memcpy(nums, tmp, n * sizeof(int));
	free(tmp);
}

int main(void)
{
	int array[2] = {1,2};
	rotate(array, 2, 1);
	int i;
	for(i=0;i<2;i++) {
		printf(">> %d\n", array[i]);
	}
	return 0;
}