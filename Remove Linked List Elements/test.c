#include <stdio.h>
#include <stdlib.h>

struct ListNode {
	int val;
	struct ListNode *next;
};

struct ListNode* removeElements(struct ListNode* head, int val) {
	struct ListNode *node, *prev;
	node = prev = head;
	while(node) {
		if (node->val == val) {
			struct ListNode *tmp = node->next;
			int flag = 0;
			if(node==head) flag=1;
			node->next = NULL;
			free(node);
			if (flag){
				head = tmp;
				prev = node = head;
			} else {
				prev->next = tmp;
				node = prev->next;
			}
			continue;
		}
		prev = node;
		node = node->next;
	}

	return head;
}

void displayElements(struct ListNode *tree)
{
	struct ListNode *parse = tree;
	printf("LIST");
	while(parse) {
		printf("->%d", parse->val);
		parse = parse->next;
	}
	printf("---\n");
}

int main(void)
{
	struct ListNode *tree = NULL;
	int i = 0, size = 10;
	for (i=0;i<size;i++) {
		struct ListNode *node = (struct ListNode *)malloc(sizeof(struct ListNode));
		node->val = 1;
		if (i%2 == 0) node->val=2;
		node->next = tree;
		tree = node;
	}
	
	displayElements(tree);
	
	tree = removeElements(tree, 1);
	
	displayElements(tree);
	
	while(tree) {
		struct ListNode *tmp = tree->next;
		tree->next = NULL;
		free(tree);
		tree = tmp;
	}
	
	return 0;
}