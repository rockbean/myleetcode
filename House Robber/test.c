#include <stdio.h>

#define MAX(a, b) (a) > (b) ? (a) : (b)

int rob(int* nums, int numsSize) {
    if (numsSize > 1) nums[1] = MAX(nums[0], nums[1]);
	int i;
    for (i = 2; i < numsSize; i++)
        nums[i] = MAX(nums[i-2] + nums[i], nums[i-1]);
    return numsSize == 0 ? 0 : nums[numsSize-1];
}

int main(void)
{
	int num[]={8,10,10,13,13,20,26};
	int i;
	printf(">>>\n");
	for(i=0;i<7;i++)  
          printf("%4d",num[i]);

	int ret = rob(num, 7);
	printf("\nret %d\n===\n", ret);
	for(i=0;i<7;i++)  
          printf("%4d",num[i]);	
	printf("<<<\n");
	return 0;
}