#include <stdio.h>
#include <limits.h>
#include <stdint.h>

int reverse_2(int x) {
long long num = x;
int n_flag = 1;
long long result = 0;
if (x < 0) {
    num = -x;
    n_flag = -1;
}
while( num > 0 ) {
    result = result * 10 + num % 10;
    num /= 10;
}
if ( result > 0x7FFFFFFF || result < (-0x7FFFFFFF - 1) ) {
    return 0;
}
return n_flag * result;
}

int reverse(int x) {
	uint64_t tmp = 0;
	uint64_t num = x;
	int flag = 1;
	
	if (x<0) {
		flag = -1;
		num = -x;
	}
	
    while(num) {
		tmp += num % 10;
		num =num /10;
		if (num)
			tmp = tmp * 10;
		if (tmp > INT_MAX - 1)
			return 0;
	}

	return flag*tmp;
}

int main(void)
{
	int ret = reverse(1534236469);
	printf("ret %d\n", ret);
	return 0;
}